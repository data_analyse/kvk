CREATE TABLE d_kvk.kvk_dataprep AS (
SELECT 
    ROW_NUMBER() OVER() as id_kvk,
	naam_kvk,
    adres_kvk,
	REPLACE(SUBSTRING (
		adres_kvk,
		'(\s[0-9]{1,5})'
	),' ','') ::BIGINT AS huisnummer_kvk,
	CASE
		WHEN SUBSTRING(REGEXP_REPLACE(SUBSTRING (REGEXP_REPLACE(
		adres_kvk,'\-',''),
		'(\s[0-9]{1,5}.*)'
	),'\s[0-9]{1,5}',''),'\s([A-Z])(\s|\-|$)') = 'I' THEN NULL
	ELSE SUBSTRING(REGEXP_REPLACE(UPPER(SUBSTRING (REGEXP_REPLACE(
		adres_kvk,'\-',''),
		'(\s[0-9]{1,5}.*)'
	)),'\s[0-9]{1,5}',''),'\s([A-Z])(\s|\-|$)')
	END as huisletter_kvk,
	CASE
		WHEN SUBSTRING(REGEXP_REPLACE(SUBSTRING (REGEXP_REPLACE(
		adres_kvk,'\-',''),
		'(\s[0-9]{1,5}.*)'
	),'\s[0-9]{1,5}',''),'\s([A-Z])(\s|\-|$)') = 'I' THEN 'I'
	ELSE REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(UPPER(SUBSTRING (REGEXP_REPLACE(
		adres_kvk,'-',''),
		'(\s[0-9]{1,5}.*)'
	)),'\s[0-9]{1,5}',''),'\s([A-Z])(\s|\-|$)',''),'^\s','')
	END as toevoeging_kvk,
    REPLACE(UPPER(postcode_kvk),' ','') as postcode_kvk,
    REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(INITCAP(plaats_kvk),'^''?(S\-|S\s)','''s ','i'),'Aan De','aan de','i'),'^''?(T\-|T\s)','''t ','i') as plaats_kvk,
    kvknummer_kvk,
    vestigingsnummer_kvk
FROM d_kvk.kvk_export
);

-- Koppeling o.b.v. postcode huisnummer. Aanmaken koppelvelden voor huisletter
CREATE TABLE d_kvk.tussenstap_01 AS (
SELECT
    a.id_kvk,
    a.naam_kvk,
    a.adres_kvk,
    a.huisnummer_kvk,
    a.huisletter_kvk,
    a.toevoeging_kvk,
    a.postcode_kvk,
    a.plaats_kvk,
    a.kvknummer_kvk,
    a.vestigingsnummer_kvk,
    CASE 
        WHEN a.huisletter_kvk IS NULL THEN 'LEEG'
        ELSE a.huisletter_kvk
        END huisletter_kvk_koppel,
    UPPER(CASE 
        WHEN a.toevoeging_kvk IS NULL THEN 'LEEG'
        WHEN a.toevoeging_kvk = '' THEN 'LEEG'
        ELSE a.toevoeging_kvk
        END) toevoeging_kvk_koppel,
    UPPER(CASE 
        WHEN b.hoofdadres_huisletter IS NULL THEN 'LEEG'
        ELSE b.hoofdadres_huisletter
        END) huisletter_vbo_koppel,
    UPPER(CASE 
        WHEN b.hoofdadres_huisnummertoevoeging IS NULL THEN 'LEEG'
        WHEN b.hoofdadres_huisnummertoevoeging = '' THEN 'LEEG'
        ELSE b.hoofdadres_huisnummertoevoeging
        END) toevoeging_vbo_koppel,
    CASE
		WHEN b.hoofdadres_huisnummertoevoeging IS NULL then 1
		WHEN CAST(SUBSTRING (b.hoofdadres_huisnummertoevoeging,'([0-9]{1,8})') AS BIGINT) IS NULL then 2
		WHEN CAST(SUBSTRING (b.hoofdadres_huisnummertoevoeging,'([0-9]{1,8})') AS BIGINT) = MIN(CAST(SUBSTRING (b.hoofdadres_huisnummertoevoeging,'([0-9]{1,8})') AS BIGINT)) OVER (PARTITION BY a.id_kvk) then 3
		ELSE 4
		END AS prio_toevoeging,
    b.vbo_id,
    b.pand_id,
    b.hoofdadres_straatnaam,
    b.hoofdadres_identificatie,
    b.hoofdadres_huisnummer,
    b.hoofdadres_huisletter,
    b.hoofdadres_huisnummertoevoeging,
    b.hoofdadres_postcode,
    b.hoofdadres_volledig_adres,
    b.nevenadres_identificatie,
    b.woonplaats,
    b.gemeentenaam,
    b.gm_code,
    b.provincienaam,
    b.geom_wgs,
    b.geom_rd
FROM d_kvk.kvk_dataprep a
LEFT JOIN d_bag.verblijfsobject b
on a.postcode_kvK = b.hoofdadres_postcode
and cast(a.huisnummer_kvk as BIGINT) = b.hoofdadres_huisnummer
GROUP BY
    a.id_kvk,
    a.naam_kvk,
    a.adres_kvk,
    a.huisnummer_kvk,
    a.huisletter_kvk,
    a.toevoeging_kvk,
    a.postcode_kvk,
    a.plaats_kvk,
    a.kvknummer_kvk,
    a.vestigingsnummer_kvk,
    b.vbo_id,
    b.pand_id,
    b.hoofdadres_straatnaam,
    b.hoofdadres_identificatie,
    b.hoofdadres_huisnummer,
    b.hoofdadres_huisletter,
    b.hoofdadres_huisnummertoevoeging,
    b.hoofdadres_postcode,
    b.hoofdadres_volledig_adres,
    b.nevenadres_identificatie,
    b.woonplaats,
    b.gemeentenaam,
    b.gm_code,
    b.provincienaam,
    b.geom_wgs,
    b.geom_rd
)
;

-- Prioriteren naar best mogelijke koppeling op huisletter en toevoeging
CREATE TABLE d_kvk.tussenstap_02 AS (
select
*
, count(*) OVER (partition by id_kvk) as aantal_koppelingen
, case
-- Koppel huisnummer, huisletter en toevoeging zijn gelijk
when vbo_id is not null and huisletter_vbo_koppel = huisletter_kvk_koppel and toevoeging_vbo_koppel = toevoeging_kvk_koppel then 1
-- Koppel huisletter is gelijk, toevoeging negeren en anders laagste nummer in toevoeging, wel VBO gekoppeld
when vbo_id is not null and huisletter_vbo_koppel = huisletter_kvk_koppel and prio_toevoeging = 1 then 2
when vbo_id  is not null and huisletter_vbo_koppel = huisletter_kvk_koppel and prio_toevoeging = 2 then 3
when vbo_id  is not null and huisletter_vbo_koppel = huisletter_kvk_koppel and prio_toevoeging = 3 then 4
-- Huisletter A koppelen aan VBO zonder huisletter, wel VBO gekoppeld 
when vbo_id  is not null and huisletter_kvk_koppel  = 'A' and huisletter_vbo_koppel = 'LEEG' and prio_toevoeging = 1 then 5
when vbo_id  is not null and huisletter_kvk_koppel  = 'A' and huisletter_vbo_koppel = 'LEEG'  and prio_toevoeging = 2 then 6
when vbo_id  is not null and huisletter_kvk_koppel  = 'A' and huisletter_vbo_koppel = 'LEEG'  and prio_toevoeging = 3 then 7
-- Huisletter anders koppelen aan VBO zonder huisletter, wel VBO gekoppeld 
when vbo_id  is not null and huisletter_kvk_koppel  != 'LEEG' and huisletter_vbo_koppel = 'LEEG'  and prio_toevoeging = 1 then 8
when vbo_id  is not null and huisletter_kvk_koppel  != 'LEEG' and huisletter_vbo_koppel = 'LEEG'  and prio_toevoeging = 2 then 9
when vbo_id  is not null and huisletter_kvk_koppel  != 'LEEG' and huisletter_vbo_koppel = 'LEEG'  and prio_toevoeging = 3 then 10
-- Huisletter is leeg koppelen aan VBO met huisletter A, wel VBO gekoppeld 
when vbo_id  is not null and huisletter_kvk_koppel = 'LEEG' and huisletter_vbo_koppel = 'A' and prio_toevoeging = 1  then 11
when vbo_id  is not null and huisletter_kvk_koppel = 'LEEG' and huisletter_vbo_koppel = 'A' and prio_toevoeging = 2  then 12
when vbo_id  is not null and huisletter_kvk_koppel = 'LEEG' and huisletter_vbo_koppel = 'A' and prio_toevoeging = 3  then 13
-- Anders handmatig opzoeken
else 14
end as koppeling_huisletter
from d_kvk.tussenstap_01
order by id_kvk
)
;

-- Bepaal koppeling met hoogste prio
CREATE TABLE d_kvk.tussenstap_03 AS (
select
*
, Min(koppeling_huisletter) OVER (partition by id_kvk) as koppeling_prio
from d_kvk.tussenstap_02
order by id_kvk
);

-- Selecteer koppeling met hoogste prio
CREATE TABLE d_kvk.tussenstap_04 AS (
select
    id_kvk,
    naam_kvk,
    adres_kvk,
    huisnummer_kvk,
    huisletter_kvk,
    toevoeging_kvk,
    postcode_kvk,
    plaats_kvk,
    kvknummer_kvk,
    vestigingsnummer_kvk,
    vbo_id,
    pand_id,
    hoofdadres_straatnaam,
    hoofdadres_identificatie,
    hoofdadres_huisnummer,
    hoofdadres_huisletter,
    hoofdadres_huisnummertoevoeging,
    hoofdadres_postcode,
    hoofdadres_volledig_adres,
    nevenadres_identificatie,
    woonplaats,
    gemeentenaam,
    gm_code,
    provincienaam,
    geom_wgs,
    geom_rd,
    koppeling_prio,
    count(*) OVER (partition by id_kvk) as aantal_koppelingen
from d_kvk.tussenstap_03
where koppeling_prio = koppeling_huisletter
-- De minst goede koppeling niet meenemen
AND koppeling_prio < 14
GROUP BY
    id_kvk,
    naam_kvk,
    adres_kvk,
    huisnummer_kvk,
    huisletter_kvk,
    toevoeging_kvk,
    postcode_kvk,
    plaats_kvk,
    kvknummer_kvk,
    vestigingsnummer_kvk,
    vbo_id,
    pand_id,
    hoofdadres_straatnaam,
    hoofdadres_identificatie,
    hoofdadres_huisnummer,
    hoofdadres_huisletter,
    hoofdadres_huisnummertoevoeging,
    hoofdadres_postcode,
    hoofdadres_volledig_adres,
    nevenadres_identificatie,
    woonplaats,
    gemeentenaam,
    gm_code,
    provincienaam,
    geom_wgs,
    geom_rd,
    koppeling_prio
order by aantal_koppelingen DESC
, id_kvk
);


-- Eindtabel
-- Join met starttabel dataprep KvK, zodat bedrijven waarvoor geen match in de BAG is, niet ontbreken.
CREATE TABLE d_kvk.kvk_geo_bag AS (
SELECT
    a.id_kvk,
    a.naam_kvk,
    a.adres_kvk,
    a.huisnummer_kvk,
    a.huisletter_kvk,
    a.toevoeging_kvk,
    a.postcode_kvk,
    a.plaats_kvk,
    a.kvknummer_kvk,
    a.vestigingsnummer_kvk,
    b.vbo_id,
    b.pand_id,
    b.hoofdadres_straatnaam,
    b.hoofdadres_identificatie,
    b.hoofdadres_huisnummer,
    b.hoofdadres_huisletter,
    b.hoofdadres_huisnummertoevoeging,
    b.hoofdadres_postcode,
    b.hoofdadres_volledig_adres,
    b.nevenadres_identificatie,
    b.woonplaats,
    b.gemeentenaam,
    b.gm_code,
    b.provincienaam,
    b.geom_wgs,
    b.geom_rd,
    b.koppeling_prio,
    b.aantal_koppelingen
FROM d_kvk.kvk_dataprep a
LEFT JOIN d_kvk.tussenstap_04 b
ON a.id_kvk = b.id_kvk
);

-- Check ontbrekende koppelingen
-- Wel in adressentabel
CREATE TABLE d_kvk.kvk_geo_ontbrekend AS (
SELECT
b.type,
b.geom,
a.*
FROM d_kvk.kvk_geo_bag a
LEFT JOIN d_bag.adres_ontdubbeld b
ON UPPER(a.postcode_kvk) = UPPER(b.postcode)
AND a.huisnummer_kvk = b.huisnummer
AND UPPER(a.huisletter_kvk) = UPPER(b.huisletter)
WHERE vbo_id IS NULL
	)
;
